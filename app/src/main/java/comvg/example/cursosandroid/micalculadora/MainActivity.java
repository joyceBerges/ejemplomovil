package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtNum1, txtNum2, txtResul;
    private Button btnSuma, btnResta, btnMulti, btnDiv, btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.initComponts();

    }

    public void initComponts() {
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResul = (EditText) findViewById(R.id.txtResul);

        btnSuma = (Button) findViewById(R.id.btnSuma);
        btnResta = (Button) findViewById(R.id.btnResta);
        btnDiv = (Button) findViewById(R.id.btnDivi);
        btnMulti = (Button) findViewById(R.id.btnMult);

        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        this.setEventos();
    }

    public void setEventos() {
        this.btnSuma.setOnClickListener(this);
        this.btnResta.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btnLimpiar.getId()) {
            txtNum1.setText("");
            txtNum2.setText("");
            txtResul.setText("");
        } else if (view.getId() == btnCerrar.getId()) {
            finish();
        } else if (txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")) {
            Toast.makeText(MainActivity.this, "Faltó capturar", Toast.LENGTH_LONG).show();
        } else {
            op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
            op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
            switch (view.getId()) {
                case R.id.btnSuma:
                    txtResul.setText(String.valueOf(op.suma()));
                    break;
                case R.id.btnResta:
                    txtResul.setText(String.valueOf(op.resta()));
                    break;
                case R.id.btnMult:
                    txtResul.setText(String.valueOf(op.mult()));
                    break;
                case R.id.btnDivi:
                    txtResul.setText(String.valueOf(op.div()));
            }
        }
    }
}
